# Generate a PlantUML diagram showing the tree of URL resolution in a
# Django project. This file to be run under the virtualenv of the target
# Django project.

import importlib
import os
from pdb import set_trace as bp
import re
import sys
import uuid

import django
from django.core.urlresolvers import RegexURLResolver, RegexURLPattern
from django.test.client import RequestFactory

class URLTree():
    def __init__(self, django_proj_path, django_proj_name,
                 excluded_url_resolver_patterns=[]):
        self.django_proj_path = django_proj_path
        self.django_proj_name = django_proj_name
        self.django_settings_module = '.'.join([django_proj_name, 'settings'])
        self.excluded_url_resolver_patterns = excluded_url_resolver_patterns
        self.url_patterns = None

    def plant_uml_text(self):
        '''Create Plant UML text which when rendered shows the URL tree.'''
        url_patterns = self._get_url_patterns()
        url_pattern_tree = self._create_pattern_tree(url_patterns)
        url_pattern_tree = self._populate_url_redirects(url_pattern_tree,
                                                        url_pattern_tree)
        url_pattern_tree = self._add_top_level_parent(url_pattern_tree)
        url_pattern_tree = self._pad_redundant_patterns(url_pattern_tree)
        plant_uml_text = self._create_plant_uml_text(url_pattern_tree)
        plant_uml_text = self._cleanup_plant_uml_text(plant_uml_text)
        return plant_uml_text

    def _get_url_patterns(self):
        # Change into the project directory. As with biblist, I'm not
        # sure how to work Django otherwise. If not done,
        # groupmaker_site.settings cannot be imported (modifications to
        # sys.path notwithstanding). The import of groupmaker must do
        # some black magic that requires the project to be cwd.
        os.chdir(self.django_proj_path)

        # Set the DJANGO_SETTINGS_MODULE environment variable. If this
        # is not done then django.setup() fails.
        settings_module_path = '.'.join([self.django_proj_name, 'settings'])
        os.environ['DJANGO_SETTINGS_MODULE'] = settings_module_path

        # Set up Django.
        django.setup()
        
        module_name = '.'.join([self.django_proj_name, 'urls'])
        module =  importlib.import_module(module_name)
        return getattr(module, 'urlpatterns')
        
    def _create_pattern_tree(self, url_patterns):
        '''Recursively create a list/dict representation of urlpatterns.'''
        tree = []
        for item in url_patterns:
            if isinstance(item, RegexURLResolver):
                if item.regex.pattern in self.excluded_url_resolver_patterns:
                    continue
                namespace = item.namespace if item.namespace else "*none*"
                patt = item.regex.pattern if item.regex.pattern else "*blank*"
                children = self._create_pattern_tree(item.url_patterns)
                tree.append({'namespace': namespace,
                             'pattern': patt,
                             'uuid': str(uuid.uuid4()).replace('-', ''),
                             'children': children})
            elif isinstance(item, RegexURLPattern):
                name = item.name if item.name else "*None*"
                redirect_url = None
                if item.callback.__name__ == 'RedirectView':
                    request = RequestFactory().get('')
                    redirect_url = item.callback(request).url

                tree.append({'pattern': item.regex.pattern,
                             'name': name,
                             'uuid': str(uuid.uuid4()).replace('-', ''),
                             'redirect_named_pattern': redirect_url,
                             'redirect_uuid': None})
            else:
                assert(False)
        return tree

    def _add_top_level_parent(self, url_pattern_tree):
        '''Add a common parent to the top-level patterns.

        This makes for a cleaner-looking plot, because otherwise the
        top-level patterns may exist at vertical positions and it is
        hard to tell they are top-level.

        '''
        tree = [{'namespace': '*none*',
                 'pattern': 'TOP LEVEL',
                 'uuid': str(uuid.uuid4()).replace('-', ''),
                 'children': url_pattern_tree}]
        return tree

    def _populate_url_redirects(self, pattern_tree, top_level_tree):
        '''Populate URL redirects recursively.'''
        for item in pattern_tree:
            if 'namespace' in item:
                self._populate_url_redirects(item['children'], top_level_tree)
            elif item['redirect_named_pattern']:
                path = item['redirect_named_pattern']
                cur_uuid = self._resolve_path_into_uuid(path, top_level_tree,
                                                        first_call=True)
                item['redirect_uuid'] = cur_uuid
        return pattern_tree

    def _resolve_path_into_uuid(self, path, tree, first_call=False):
        if first_call:
            # Peel the initial '/' off. Django does this internally by
            # setting an initial resolver like this:
            #
            # RegexURLResolver(r'^/', urlconf)
            #
            # but here we will do it manually.
            if path[0] == '/':
                path = path[1:]

        for item in tree:
            match = re.compile(item['pattern']).search(path)
            if match:
                if 'namespace' in item:
                    new_path = path[match.end():]
                    return self._resolve_path_into_uuid(new_path,
                                                        item['children'])
                else:
                    return item['uuid']

    def _pad_redundant_patterns(self, pattern_tree, pattern_set=set()):
        '''Uniquify patterns by padding with spaces.

        PlantUML assumes the object identifiers (which in this case is the
        patterns) are unique. So if you have the following PlantUML code

        package "tests" {
        *stuff in here*
        }
        ...
        package "tests" {
        *different stuff in here*
        }

        it will create one package "tests", with both *stuff* and *different
        stuff* in it. To avoid this I pad the second package name: "tests "

        '''

        for item in pattern_tree:
            while item['pattern'] in pattern_set:
                item['pattern'] = item['pattern'] + ' '
            pattern_set.add(item['pattern'])
            if 'namespace' in item:
                self._pad_redundant_patterns(item['children'], pattern_set)
        return pattern_tree

    def _create_plant_uml_text(self, pattern_tree, parent_uuid=None):
        '''Create class diagram in PlantUML from the tree.

        I wanted to make an object diagram but PlantUML had trouble with
        having non-alphanumerics in the object identifier and I couldn't
        figure out how to escape them. Elements such as

        object ^some_pattern$ {
        namespace = mynamespace
        }

        were problematic. But in class diagrams wrapping in double
        quotes worked.

        '''
        text = ''
        for item in pattern_tree:
            if 'namespace' in item:
                pattern = item['pattern'].replace('^','^')
                text += 'class "' + pattern + '" as ' + item['uuid'] + ' {\n'
                text += 'namespace = ' + item['namespace'] + '\n'
                text += '}\n\n'
                text += self._create_plant_uml_text(item['children'],
                                                    parent_uuid=item['uuid'])
            else:
                pattern = item['pattern'].replace('^','^')
                text += 'class "' + pattern + '" as ' + item['uuid'] + ' {\n'
                text += 'name = ' + item['name'] + '\n'
                text += '}\n\n'
            if parent_uuid:
                text += parent_uuid + ' --> ' + item['uuid'] + '\n\n'
            # Add redirects.
            if 'redirect_uuid' in item:
                if item['redirect_uuid'] is not None:
                    text += (item['uuid'] + ' ..> ' +
                             item['redirect_uuid'] + '\n\n')
        if parent_uuid:
            text += '\n'
        text += '\n'
        return text

    def _cleanup_plant_uml_text(self, plant_uml_text):
        # Separate redirection lines from the others, for later
        # aggregation of redirection lines at the bottom. Otherwise, if
        # a line such as
        #
        # uuid1 --> uuid2
        #
        # appears before a class declaration such as
        #
        # class "^signups$" as uuid1 {
        # name = *None*
        # }
        #
        # then instead of the element appearing with the class name
        # ^signups$ it will appear as uuid1.
        line_num = 0
        redirect_lines = []
        other_lines = []
        lines = plant_uml_text.split('\n')
        while line_num < len(lines):
            line = lines[line_num]
            match = re.compile('([0-9a-f]+ ..> [0-9a-f]+)').match(line)
            if match:
                # Add the matching line and the presumably blank newline
                # below it to the list of redirect lines.
                redirect_lines.append(line)
                redirect_lines.append('')
                line_num += 2
            else:
                other_lines.append(line)
                line_num += 1

        # Join non-redirect lines and remove excess blank newlines at
        # the end.
        other_text = '\n'.join(other_lines)
        deblanked_text = re.compile(r'\n\n(\n*)$').sub('\n\n', other_text)
        
        # Join the redirects to the end of the non-redirect lines.
        redirect_text = '\n'.join(redirect_lines)
        return deblanked_text + redirect_text
